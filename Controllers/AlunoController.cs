using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AlunoController : ControllerBase
  {
    private readonly Context _context;

    public AlunoController(Context context)
    {
        _context = context;
    }

    // GET api/aluno
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Aluno);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] string Id){

      return Ok(_context.Aluno.Where(x=>x.Cpf.Equals(Id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Aluno aluno){

      try
      {
        _context.Aluno.Add(aluno);
        _context.SaveChanges();
        return Ok(aluno.Cpf);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(string id, Aluno aluno){
      if (id != aluno.Cpf){
        return BadRequest();
      }

      _context.Entry(aluno).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(string id){
      var aluno = _context.Aluno.Find(id);

      if (aluno == null){
        return NotFound();
      }

      _context.Aluno.Remove(aluno);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}