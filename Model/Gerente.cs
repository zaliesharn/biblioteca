using System.ComponentModel.DataAnnotations;

public class Gerente {
    [Key]
    public int Id { get; set; }
    public string Nome { get; set; }
    public string Cpf { get; set; }
}